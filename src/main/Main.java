package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Deque;
import java.util.LinkedList;

import decorator.Color;
import decorator.EnumColor;
import decorator.JugadorDecorator;
import decorator.JugadorImpl;
import decorator.ObjectesMillorarDecorador;
import decorator.VehiclesDecorador;
import factory.EnemicsFactory;
import factory.EnumJugador;
import factory.FactoryProvider;
import factory.JocFactory;
import factory.JugadorFactory;
import factory.ObjectesBonusFactory;
import factory.ObjectesCacarFactory;
import factory.ObjectesMillorarFactory;
import factory.VehiclesFactory;
import objetos.Enemics;
import objetos.Objecte;
import objetos.ObjectesBonus;
import objetos.ObjectesCacar;
import objetos.ObjectesMillorar;
import objetos.Vehicles;
import observer.ColectorPunts;
import observer.Jugador;
import observer.ObjecteGuanyat;

public class Main {

	public static void main(String[] args) {

		// CREAMOS JUGADORES
		JugadorFactory jf = new JugadorFactory();
		Deque pila = new LinkedList<Jugador>();

		// -------------------------------PARA PRUEBAS--------------------------------//
//		Jugador j1 = jf.crearJugador(EnumJugador.GUERRER, "G1", 0);
//		Jugador j2 = jf.crearJugador(EnumJugador.COMERCIANT, "C1", 20);
//		Jugador j3 = jf.crearJugador(EnumJugador.MAG, "M1", 15);
//		Jugador j4 = jf.crearJugador(EnumJugador.COMERCIANT, "C2", 100);
//		Jugador j5 = jf.crearJugador(EnumJugador.MAG, "M2", 90);
//		Jugador j6 = jf.crearJugador(EnumJugador.GUERRER, "G2", 8);
		// ---------------------------------------------------------------------------//

		//Creamos jugadores
		Jugador j1 = jf.crearJugador(EnumJugador.GUERRER, "Guerrer 1", 0);
		Jugador j2 = jf.crearJugador(EnumJugador.COMERCIANT, "Comerciant 1", 0);
		Jugador j3 = jf.crearJugador(EnumJugador.MAG, "Mag 1", 0);
		Jugador j4 = jf.crearJugador(EnumJugador.COMERCIANT, "Comerciant 2", 0);
		Jugador j5 = jf.crearJugador(EnumJugador.MAG, "Mag 2", 0);
		Jugador j6 = jf.crearJugador(EnumJugador.GUERRER, "Guerrer 2", 0);
		
		//Añadimos jugadores a la pila
		pila.addFirst(j1);
		pila.addFirst(j2);
		pila.addFirst(j3);
		pila.addFirst(j4);
		pila.addFirst(j5);
		pila.addFirst(j6);

		
		//Creamos conexion
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");


			stmt = conn.createStatement();
			
			
			for (Object jugador : pila) {
				//stmt.executeUpdate("INSERT INTO jugador (nom, punts) VALUES ('" + ((Jugador) jugador).getNom() +"', " + (double)((Jugador) jugador).getPunts() + "); ");
			}
			
			rs = stmt.executeQuery("SELECT * FROM jugador");
			
			while (rs.next()) {
				// Display values
				System.out.print("ID: " + rs.getInt("idjugador")+"; ");
				System.out.print("Name: " + rs.getString("nom"));
				System.out.println();

			}
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}
		}

		//System.out.println("JUGADORS: " + pila);

		// FACTORY
		// Enemigos
		EnemicsFactory ef = new EnemicsFactory();

		Enemics e1 = ef.create("Pirata", "Jack Sparrow");
		Enemics e2 = ef.create("Monstre", "Monstruo del Lago Ness");
		Enemics e3 = ef.create("Bestia", "Bestia");

		System.out.println(e1);
		System.out.println(e2);
		System.out.println(e3);

		// Objectes cacar
		ObjectesCacarFactory ocf = new ObjectesCacarFactory();

		ObjectesCacar oc1 = ocf.create("Moneda", "Moneda 01");
		ObjectesCacar oc2 = ocf.create("LingotOr", "Lingote 01");
		ObjectesCacar oc3 = ocf.create("CofreDelTresor", "Cofre del tresor 01");

		System.out.println(oc1);
		System.out.println(oc2);
		System.out.println(oc3);

		// Objectes bonus
		ObjectesBonusFactory obf = new ObjectesBonusFactory();

		ObjectesBonus ob1 = obf.create("Hamburguesa", "Burger");
		ObjectesBonus ob2 = obf.create("Diamant", "Brillito");
		ObjectesBonus ob3 = obf.create("RedBull", "Alas");

		System.out.println(ob1);
		System.out.println(ob2);
		System.out.println(ob3);

		// Objectes millorar
		ObjectesMillorarFactory omf = new ObjectesMillorarFactory();

		ObjectesMillorar om1 = omf.create("Espasa", "Espasa 01");
		ObjectesMillorar om2 = omf.create("Pistola", "Pistola 01");
		ObjectesMillorar om3 = omf.create("Laxant", "Laxant 01");		
		
		System.out.println(om1);
		System.out.println(om2);
		System.out.println(om3);		
		
		// Vehicles
		VehiclesFactory vf = new VehiclesFactory();

		Vehicles v1 = vf.create("Patinet", "Patinet 01");
		Vehicles v2 = vf.create("Drac", "Desdentao");

		// FACTORIES
		// Enemics
		JocFactory factoryEnemics;
		factoryEnemics = FactoryProvider.getFactory("Enemics");
		Objecte enemicPirata = (Objecte) factoryEnemics.create("Pirata", "Jackkk Sparrowwwww");

		System.out.println(enemicPirata);

		// Vehicles
		JocFactory factoryVehicles;
		factoryVehicles = FactoryProvider.getFactory("Vehicles");
		Objecte vehicleBarca = (Objecte) factoryVehicles.create("Barca", "Barca nom");
		Objecte vehiclePatinet = (Objecte) factoryVehicles.create("Patinet", "Patinet nom");

		System.out.println(vehicleBarca);
		System.out.println(vehiclePatinet);

		System.out.println();
		System.out.println("***************************************");

		// OBSERVER
		System.out.println("-----OBSERVER + ADAPTER-----");
		ColectorPunts colector = new ColectorPunts();
		System.out.println("Inicio de jugador 1: " + j1);

		// Guerrer
		System.out.println("Guerrer agafa objecte");
		colector.addPropertyChangeListener(j1);
		ObjecteGuanyat objecte = new ObjecteGuanyat(j1, oc1);
		ObjecteGuanyat objecte2 = new ObjecteGuanyat(j1, e1);

		// Mag
		System.out.println("Mag agafa objecte");
		colector.addPropertyChangeListener(j3);
		ObjecteGuanyat objecte4 = new ObjecteGuanyat(j3, oc1);

		// Comerciant
		System.out.println("Comerciant agafa objecte");
		colector.addPropertyChangeListener(j2);
		ObjecteGuanyat objecte5 = new ObjecteGuanyat(j2, oc1);

		colector.setPunts(objecte);
		System.out.println("Guerrer nous punts amb objecte cacar: " + j1);
		colector.setPunts(objecte2);
		System.out.println("Guerrer nous punts amb enemic: " + j1);
		colector.setPunts(objecte4);
		System.out.println("Mag nous punts amb objecte cacar: " + j3);
		colector.setPunts(objecte5);
		System.out.println("Comerciant nous punts amb objecte cacar: " + j2);
		System.out.println();

		// DECORATOR
		System.out.println("-----DECORATOR-----");
		System.out.println("**Al J1 se le añade un color**");
		j1 = new Color(new JugadorImpl(j1), EnumColor.AQUA);
		System.out.println(j1.descripcio());

		System.out.println("**Al J1 se le añade un color y un objecteMillorar***");
		j1 = new ObjectesMillorarDecorador(new Color(new JugadorImpl(j1), EnumColor.MEDIUMSPRINGGREEN), om1);
		System.out.println(j1.descripcio());
		System.out.println(((JugadorDecorator) j1).getFactor());

		System.out.println("***Al J1 se le añade un color, un objecteMillorar y un Vehicle***");
		j1 = new VehiclesDecorador(
				new ObjectesMillorarDecorador(new Color(new JugadorImpl(j1), EnumColor.MEDIUMSPRINGGREEN), om1), v2);
		System.out.println(j1.descripcio());
		System.out.println(((VehiclesDecorador) j1).getFactor());

		System.out.println("***Al J2 se le añade un color, un objecteMillorar y un Vehicle***");
		j2 = new VehiclesDecorador(
				new ObjectesMillorarDecorador(new Color(new JugadorImpl(j2), EnumColor.MEDIUMSPRINGGREEN), om2), v2);
		System.out.println(j2.descripcio());
		System.out.println(((VehiclesDecorador) j2).getFactor());
		
		System.out.println("***Al J3 se le añade un color, un objecteMillorar y un Vehicle***");
		j3 = new VehiclesDecorador(
				new ObjectesMillorarDecorador(new Color(new JugadorImpl(j3), EnumColor.MEDIUMSPRINGGREEN), om3), v2);
		System.out.println(j3.descripcio());
		System.out.println(((VehiclesDecorador) j3).getFactor());

		System.out.println();

		// COGIENDO OBJETOS
		System.out.println("-----COGIENDO OBJETOS CON PERSONAJE QUE TIENE OBJETOSMILLORAR-----");
		System.out.println("J1 coge un enemigo");
		System.out.println("Punts abans:" + j1.getPunts());
		ObjecteGuanyat objecte3 = new ObjecteGuanyat(j1, e1);
		colector.setPunts(objecte3);
		System.out.println("Punts després " + j1.getPunts());

		System.out.println("J2 coge un enemigo");
		System.out.println("Punts abans:" + j2.getPunts());
		ObjecteGuanyat objecte33 = new ObjecteGuanyat(j2, e1);
		colector.setPunts(objecte33);
		System.out.println("Punts després " + j2.getPunts());

		System.out.println("J3 coge un objecteBonus");
		System.out.println("Punts abans:" + j3.getPunts());
		ObjecteGuanyat objecte6 = new ObjecteGuanyat(j3, ob1);
		colector.setPunts(objecte6);
		System.out.println("Punts després " + j3.getPunts());
		
	}
}
