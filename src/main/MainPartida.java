package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import decorator.Color;
import decorator.EnumColor;
import decorator.JugadorImpl;
import decorator.ObjectesMillorarDecorador;
import decorator.VehiclesDecorador;
import factory.EnumJugador;
import factory.FactoryProvider;
import factory.JocFactory;
import factory.JugadorFactory;
import jdbc.JugadorJDBC;
import objetos.Enemics;
import objetos.ObjectesBonus;
import objetos.ObjectesCacar;
import objetos.ObjectesMillorar;
import objetos.Vehicles;
import observer.ColectorPunts;
import observer.Jugador;
import observer.ObjecteGuanyat;

public class MainPartida {

	public static void main(String[] args) {

		
		// Factories
		JugadorFactory jf = new JugadorFactory();
		JocFactory factoryEnemics = FactoryProvider.getFactory("Enemics");
		JocFactory factoryVehicles = FactoryProvider.getFactory("Vehicles");
		JocFactory factoryObjectesCacar = FactoryProvider.getFactory("ObjectesCacar");
		JocFactory factoryObjectesBonus = FactoryProvider.getFactory("ObjectesBonus");
		JocFactory factoryObjectesMillorar = FactoryProvider.getFactory("ObjectesMillorar");
		/*
		 * EnemicsFactory ef = new EnemicsFactory(); ObjectesCacarFactory ocf = new
		 * ObjectesCacarFactory(); ObjectesBonusFactory obf = new
		 * ObjectesBonusFactory(); ObjectesMillorarFactory omf = new
		 * ObjectesMillorarFactory(); VehiclesFactory vf = new VehiclesFactory();
		 */

		// Creamos jugadores
		Deque pila = new LinkedList<Jugador>();
		Jugador j1 = jf.crearJugador(EnumJugador.GUERRER, "Guerrer 1", 0);
		Jugador j2 = jf.crearJugador(EnumJugador.COMERCIANT, "Comerciant 1", 0);
		Jugador j3 = jf.crearJugador(EnumJugador.MAG, "Mag 1", 0);
		

		// Observable
		ColectorPunts colector = new ColectorPunts();
				
		colector.addPropertyChangeListener((Jugador) j1);
		System.out.println("Añadiendo jugador "+j1.toString());
		colector.addPropertyChangeListener((Jugador) j2);
		System.out.println("Añadiendo jugador "+j2.toString());
		colector.addPropertyChangeListener((Jugador) j3);
		System.out.println("Añadiendo jugador "+j3.toString()+"\n");
		
		
		// Creacion de objectesMillorar
		ArrayList<ObjectesMillorar> objectesMillorarList = new ArrayList<>();
		ObjectesMillorar om1 = (ObjectesMillorar) factoryObjectesMillorar.create("Espasa", "Espasa 01");
		ObjectesMillorar om2 = (ObjectesMillorar) factoryObjectesMillorar.create("Pistola", "Pistola 01");
		ObjectesMillorar om3 = (ObjectesMillorar) factoryObjectesMillorar.create("Laxant", "Laxant 01");
		ObjectesMillorar om4 = (ObjectesMillorar) factoryObjectesMillorar.create("ExerciciEloi", "ExerciciEloi100");
		objectesMillorarList.add(om1);
		objectesMillorarList.add(om2);
		objectesMillorarList.add(om3);
		objectesMillorarList.add(om4);

		// Damos objetos a los jugadores
		j1 = new ObjectesMillorarDecorador(new JugadorImpl(j1), om1);
		j2 = new ObjectesMillorarDecorador(new JugadorImpl(j2), om2);
		j3 = new ObjectesMillorarDecorador(new JugadorImpl(j3), om3);


		// Añadimos jugadores a la pila
		pila.addFirst(j1);
		pila.addFirst(j2);
		pila.addFirst(j3);
		
		// Añadimos a la base de datos
		insertarRegistros(pila);
		consultarRegistros();

		// Creamos objetos
		ArrayList<Enemics> enemicsList = new ArrayList<>();
		Enemics e1 = (Enemics) factoryEnemics.create("Pirata", "Jack Sparrow");
		Enemics e2 = (Enemics) factoryEnemics.create("Monstre", "Monstruo del Lago Ness");
		Enemics e3 = (Enemics) factoryEnemics.create("Bestia", "Bestia");
		enemicsList.add(e1);
		enemicsList.add(e2);
		enemicsList.add(e3);

		ArrayList<ObjectesCacar> objectesCacarList = new ArrayList<>();
		ObjectesCacar oc1 = (ObjectesCacar) factoryObjectesCacar.create("Moneda", "Moneda 01");
		ObjectesCacar oc2 = (ObjectesCacar) factoryObjectesCacar.create("LingotOr", "Lingote 01");
		ObjectesCacar oc3 = (ObjectesCacar) factoryObjectesCacar.create("CofreDelTresor", "Cofre del tresor 01");
		objectesCacarList.add(oc1);
		objectesCacarList.add(oc2);
		objectesCacarList.add(oc3);

		ArrayList<ObjectesBonus> objectesBonusList = new ArrayList<>();
		ObjectesBonus ob1 = (ObjectesBonus) factoryObjectesBonus.create("Hamburguesa", "Burger");
		ObjectesBonus ob2 = (ObjectesBonus) factoryObjectesBonus.create("Diamant", "Brillito");
		ObjectesBonus ob3 = (ObjectesBonus) factoryObjectesBonus.create("RedBull", "Alas");
		objectesBonusList.add(ob1);
		objectesBonusList.add(ob2);
		objectesBonusList.add(ob3);

		ArrayList<Vehicles> vehiclesList = new ArrayList<>();
		Vehicles v1 = (Vehicles) factoryVehicles.create("Patinet", "Patinet 01");
		Vehicles v2 = (Vehicles) factoryVehicles.create("Drac", "Desdentao");
		Vehicles v3 = (Vehicles) factoryVehicles.create("Ala delta", "Ala delta 01");
		Vehicles v4 = (Vehicles) factoryVehicles.create("Barca", "Barca 01");
		Vehicles v5 = (Vehicles) factoryVehicles.create("Cotxe", "Cotxe 01");
		vehiclesList.add(v1);
		vehiclesList.add(v2);
		vehiclesList.add(v3);
		vehiclesList.add(v4);
		vehiclesList.add(v5);


		// Bucle de partida
		Random r = new Random();
		int n = 50;
		System.out.println("\n -------------- INICIA LA PARTIDA --------------------");
		for (int i = 0; i < n; i++) {
			// Mou la cua tantes vegades com diu el dau
			int dado = r.nextInt(1, 7);
			System.out.println("Valor del dado: "+dado);
			giraPila(dado, pila);

			// Valor aleatori per escollir acció del torn
			int num = r.nextInt(1, 101);
			Jugador j = (Jugador) pila.pop();
			System.out.println("\n---------------------------------------------------------");
			System.out.println("Es el turno del jugador: "+j.getNom()+" y tiene puntos: "+j.getPunts());

			if (num <= 30) { // Ganar un objecteCaçar
				int posicio = r.nextInt(0, objectesCacarList.size());
				ObjectesCacar obj = objectesCacarList.get(posicio);
				ObjecteGuanyat objecte = new ObjecteGuanyat(j, obj);
				System.out.println("Le ha tocado un objecte caçar: "+obj.toString());
				colector.setPunts(objecte);
			} else if (num <= 60) { // Ganar un objecteBonus

				int posicio = r.nextInt(0, objectesBonusList.size());
				ObjectesBonus obj = objectesBonusList.get(posicio);
				ObjecteGuanyat objecte = new ObjecteGuanyat(j, obj);
				System.out.println("Le ha tocado un objecte bonus: "+obj.toString());
				colector.setPunts(objecte);
			}

			else if (num <= 90) { // Ganar un enemic

				int posicio = r.nextInt(0, enemicsList.size());
				Enemics obj = enemicsList.get(posicio);
				ObjecteGuanyat objecte = new ObjecteGuanyat(j, obj);
				System.out.println("Le ha tocado un enemic: "+obj.toString());
				colector.setPunts(objecte);
			}

			else if (num <= 94) { // Ganar un objecteMillorar

				int posicio = r.nextInt(0, objectesMillorarList.size());
				ObjectesMillorar obj = objectesMillorarList.get(posicio);
				j = new ObjectesMillorarDecorador(new JugadorImpl(j), obj);
				System.out.println("Le ha tocado un objecte millorar: "+obj.toString());

			} else if (num <= 97) { // Ganar un color
				EnumColor[] colores = EnumColor.values();
				int posicio = r.nextInt(0, colores.length - 1);
				EnumColor color = colores[posicio];
				j = new Color(new JugadorImpl(j), color);
				System.out.println("Le ha tocado un color: "+color.toString());
				System.out.println("JugadorDecorator: "+j);
				
			} else { // Ganar un vehicle

				int posicio = r.nextInt(0, vehiclesList.size());
				Vehicles obj = vehiclesList.get(posicio);
				j = new VehiclesDecorador(new JugadorImpl(j), obj);
				System.out.println("Le ha tocado un vehicle: "+obj.toString());
				System.out.println("JugadorDecorator: "+j);
			}
			
			System.out.println("Fin del turno. Puntos actuales: "+j.getPunts());
			System.out.println("---------------------------------------------------------");
			
			pila.addFirst(j);
						
		}
		
		for (Object j : pila) {
			colector.removePropertyChangeListener((Jugador) j);
		}

		resultadosFinales(pila);
		
	}
	
	
	private static void resultadosFinales(Deque pila) {
		System.out.println("\n-----Jugadores antes de la actualización-----");
		consultarRegistros();
		for (Object j : pila) {
			Jugador jugador = (Jugador) j;
			if(jugador.getPunts()>=500) {
				updateRegistros(jugador);
			}
			else {
				borrarRegistros(jugador);
			}
			System.out.println("\n-----Jugadores después de la actualización-----");
			consultarRegistros();
		}
		ArrayList<JugadorJDBC> listaJDBC = obtenerRegistros();
		SortedSet<JugadorJDBC> sortedJDBC = new TreeSet<JugadorJDBC>();
		sortedJDBC.addAll(listaJDBC);		
		System.out.println("\n-----Jugadores ordenados alfabeticamente-----");
		for (JugadorJDBC j : sortedJDBC) {
			System.out.println(j.getNom()+" Punts: "+j.getPunts());
		}
		ordenarPorPuntos(listaJDBC);
		
	}

	private static void ordenarPorPuntos(ArrayList<JugadorJDBC> listaJDBC) {
		System.out.println("\n-----Jugadores ordenados por puntos-----");
		Comparator<JugadorJDBC> comparatorPuntos = new Comparator<JugadorJDBC>() {
			@Override
			public int compare(JugadorJDBC o1, JugadorJDBC o2) {
				if (o1.getPunts() == o2.getPunts())
					return 1;
				else if (o1.getPunts() < o2.getPunts())
					return 1;
				else
					return -1;
			}
		};
		
		SortedSet<JugadorJDBC> puntosDefinitivos = new TreeSet<JugadorJDBC>(comparatorPuntos);
		puntosDefinitivos.addAll(listaJDBC);
		
		for (JugadorJDBC j : puntosDefinitivos) {
			System.out.println(j.getNom()+" Punts: "+j.getPunts());
		}
		
	}
	
	
	private static void giraPila(int dado, Deque pila) {
		for (int i = 0; i < dado; i++) {
			pila.addLast(pila.pop());
		}
	}
	
	private static void borrarRegistros(Jugador j) {
		// Creamos conexion
				Connection conn = null;
				Statement stmt = null;
				ResultSet rs = null;
				try {
					conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

					stmt = conn.createStatement();
					
					System.out.println("Borrando a "+j.getNom());
					stmt.executeUpdate("DELETE FROM jugador WHERE nom='"+j.getNom()+"';");

				} catch (SQLException ex) {
					// handle any errors
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				} finally {

					if (rs != null) {
						try {
							rs.close(); // Cerrar siempre las conexiones
						} catch (SQLException sqlEx) {
						} // ignore
						rs = null;
					}
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException sqlEx) {
						} // ignore
						stmt = null;
					}
				}
	}
	
	private static void updateRegistros(Jugador j) {
		// Creamos conexion
				Connection conn = null;
				Statement stmt = null;
				ResultSet rs = null;
				try {
					conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

					stmt = conn.createStatement();

					System.out.println("Actualizando a jugador "+j.getNom());
					rs = stmt.executeQuery("SELECT punts FROM jugador WHERE nom='"+j.getNom()+"';");
					rs.next();
					float puntsJ = rs.getInt("punts")+j.getPunts();
					stmt.executeUpdate("UPDATE jugador SET punts="+puntsJ+"WHERE nom='"+j.getNom()+"';");

				} catch (SQLException ex) {
					// handle any errors
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				} finally {

					if (rs != null) {
						try {
							rs.close(); // Cerrar siempre las conexiones
						} catch (SQLException sqlEx) {
						} // ignore
						rs = null;
					}
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException sqlEx) {
						} // ignore
						stmt = null;
					}
				}
	}

	private static void insertarRegistros(Deque pila) {
		// Creamos conexion
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

			stmt = conn.createStatement();

			for (Object jugador : pila) {
				rs = stmt.executeQuery("SELECT * FROM jugador;");
				boolean exists = false;

				while (rs.next()) {
					if (rs.getString("nom").equals(((Jugador) jugador).getNom())) {
						exists = true;
					}
				}

				if (!exists) {
					stmt.executeUpdate("INSERT INTO jugador (nom, punts) VALUES ('" + ((Jugador) jugador).getNom()
							+ "', " + (double) ((Jugador) jugador).getPunts() + "); ");
				}
			}

		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {

			if (rs != null) {
				try {
					rs.close(); // Cerrar siempre las conexiones
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}
		}
	}

	private static void consultarRegistros() {
		// Creamos conexion
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

			stmt = conn.createStatement();

			rs = stmt.executeQuery("SELECT * FROM jugador");

			System.out.println("-----JUGADORES EN BASE DE DATOS-----");
			while (rs.next()) {
				// Display values
				System.out.print("ID: " + rs.getInt("idjugador") + "; ");
				System.out.print("Name: " + rs.getString("nom")+"; ");
				System.out.print("Punts: " + rs.getString("punts"));
				System.out.println();

			}
			System.out.println("-----------------------------------------");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {

			if (rs != null) {
				try {
					rs.close(); // Cerrar siempre las conexiones
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}
		}
	}
	
	private static ArrayList<JugadorJDBC> obtenerRegistros(){
		//Creamos un Arraylist para guardar los resultados
		ArrayList<JugadorJDBC> listaJugadors = new ArrayList<>();
		// Creamos conexion
				Connection conn = null;
				Statement stmt = null;
				ResultSet rs = null;
				try {
					conn = DriverManager.getConnection("jdbc:mysql://localhost/videojocjdbc?" + "user=root&password=super3");

					stmt = conn.createStatement();

					rs = stmt.executeQuery("SELECT * FROM jugador");

					while (rs.next()) {
						JugadorJDBC jJDBC = new JugadorJDBC(rs.getString("nom"), (float)rs.getDouble("punts")) ;
						listaJugadors.add(jJDBC);
					}
				} catch (SQLException ex) {
					// handle any errors
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				} finally {

					if (rs != null) {
						try {
							rs.close(); // Cerrar siempre las conexiones
						} catch (SQLException sqlEx) {
						} // ignore
						rs = null;
					}
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException sqlEx) {
						} // ignore
						stmt = null;
					}
				}
		
		return listaJugadors;
		
	}

}
