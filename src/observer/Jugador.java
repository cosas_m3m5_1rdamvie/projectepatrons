package observer;

import java.beans.PropertyChangeListener;

public interface Jugador extends PropertyChangeListener {

	String getNom();

	float getPunts();

	void setPunts(float punts);

	float getFactor();

	void setFactor(float factor);

	String descripcio();
}
