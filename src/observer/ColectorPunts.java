package observer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ColectorPunts {

	private PropertyChangeSupport support;
	private ObjecteGuanyat objecte;

	public ColectorPunts() {
		support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public void setPunts(ObjecteGuanyat value) {
		support.firePropertyChange("punts", this.objecte, value);
		this.objecte = value;
	}

}
