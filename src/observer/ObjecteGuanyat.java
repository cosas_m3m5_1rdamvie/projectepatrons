package observer;

import objetos.Objecte;

public class ObjecteGuanyat {

	private float punts;
	private Jugador jugador;
	private Objecte objecteGuanyat;

	public ObjecteGuanyat(Jugador jugador, Objecte obj) {
		super();
		this.jugador = jugador;
		this.objecteGuanyat = obj;

	}

	public float getPunts() {
		return punts;
	}

	public void setPunts(float punts) {
		this.punts = punts;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public Objecte getObjecteGuanyat() {
		return objecteGuanyat;
	}

	public void setObjecteGuanyat(Objecte objecteGuanyat) {
		this.objecteGuanyat = objecteGuanyat;
	}

}
