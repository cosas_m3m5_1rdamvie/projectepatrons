package decorator;

import observer.Jugador;

public abstract class JugadorDecorator implements Jugador {
	private Jugador jugador;

	public JugadorDecorator(Jugador jugador) {
		this.jugador = jugador;
	}

	public JugadorDecorator(Jugador jugador, float factor) {
		this.jugador = jugador;
		this.jugador.setFactor(factor);
	}

	@Override
	public String descripcio() {
		return jugador.descripcio();
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	@Override
	public String toString() {
		return "JugadorDecorator [jugador=" + jugador + ", factor=" + jugador.getFactor() + "]";
	}

}
