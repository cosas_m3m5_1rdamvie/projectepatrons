package decorator;

import java.beans.PropertyChangeEvent;

import objetos.Vehicles;
import observer.Jugador;

public class VehiclesDecorador extends JugadorDecorator {
	private Vehicles vehicle;

	public VehiclesDecorador(Jugador jugador, Vehicles vehicle) {
		super(jugador);
		this.vehicle = vehicle;
	}

	public String descripcio() {
		return super.descripcio() + afegirVehicle();
	}

	private String afegirVehicle() {
		return " té el vehicle " + vehicle;
	}

	@Override
	public String getNom() {
		return super.getJugador().getNom();
	}

	@Override
	public float getPunts() {
		return super.getJugador().getPunts();
	}

	@Override
	public void setPunts(float punts) {

	}

	public float getFactor() {
		return super.getJugador().getFactor();
	}

	@Override
	public void setFactor(float factor) {

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

	@Override
	public String toString() {
		return "VehiclesDecorador [vehicle=" + vehicle + "]";
	}

}
