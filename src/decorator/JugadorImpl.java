package decorator;

import java.beans.PropertyChangeEvent;

import observer.Jugador;

public class JugadorImpl implements Jugador {
	private Jugador jugador;

	public JugadorImpl(Jugador jugador) {
		super();
		this.jugador = jugador;
	}

	@Override
	public String descripcio() {
		return "El jugador " + jugador.getNom() + " amb saldo " + jugador.getPunts();
	}

	@Override
	public String getNom() {
		return jugador.getNom();
	}

	@Override
	public float getPunts() {
		return jugador.getPunts();
	}

	@Override
	public void setPunts(float saldo) {

	}

	@Override
	public float getFactor() {
		return jugador.getFactor();
	}

	@Override
	public void setFactor(float factor) {
		jugador.setFactor(factor);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

}
