package decorator;

import java.beans.PropertyChangeEvent;

import objetos.ObjectesMillorar;
import observer.Jugador;

public class ObjectesMillorarDecorador extends JugadorDecorator {
	private ObjectesMillorar objecteQueMillora;

	public ObjectesMillorarDecorador(Jugador jugador, ObjectesMillorar objecteQueMillora) {
		super(jugador, objecteQueMillora.getFactor());
		this.objecteQueMillora = objecteQueMillora;

	}

	public String descripcio() {
		return super.descripcio() + afegirObjecteQueMillora();
	}

	private String afegirObjecteQueMillora() {
		return " té l'objecte " + objecteQueMillora;
	}

	public ObjectesMillorar getObjecteQueMillora() {
		return objecteQueMillora;
	}

	public void setObjecteQueMillora(ObjectesMillorar objecteQueMillora) {
		this.objecteQueMillora = objecteQueMillora;
	}

	@Override
	public String getNom() {
		return super.getJugador().getNom();
	}

	@Override
	public float getPunts() {
		return super.getJugador().getPunts();
	}

	@Override
	public void setPunts(float saldo) {

	}

	public float getFactor() {
		return objecteQueMillora.getFactor();
	}

	@Override
	public void setFactor(float factor) {

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

	@Override
	public String toString() {
		return "ObjectesMillorarDecorador [objecteQueMillora=" + objecteQueMillora + "]";
	}
}
