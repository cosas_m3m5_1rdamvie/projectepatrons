package decorator;

import java.beans.PropertyChangeEvent;
import observer.Jugador;

public class Color extends JugadorDecorator {
	private EnumColor color;

	public Color(Jugador jugador, EnumColor color) {
		super(jugador);
		this.color = color;
	}

	public String descripcio() {
		return super.descripcio() + decorateWithColor();
	}

	private String decorateWithColor() {
		return " és de color " + color;
	}

	@Override
	public String getNom() {
		return super.getJugador().getNom();
	}

	@Override
	public float getPunts() {
		return super.getJugador().getPunts();
	}

	@Override
	public void setPunts(float saldo) {

	}

	public float getFactor() {
		return super.getJugador().getFactor();
	}

	@Override
	public void setFactor(float factor) {
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

	@Override
	public String toString() {
		return "Color [color=" + color + "]";
	}

}
