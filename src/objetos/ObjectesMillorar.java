package objetos;

public class ObjectesMillorar implements Objecte {

	private float factor;
	private String nom;
	private String tipus;

	public ObjectesMillorar(float factor, String nom, String tipus) {
		super();
		this.factor = factor;
		this.nom = nom;
		this.tipus = tipus;
	}

	@Override
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String getTipus() {
		return tipus;
	}

	public void setTipus(String tipus) {
		this.tipus = tipus;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}

	public void ferServir(Enemics e) {
		e.setPuntsARestar(e.getPuntsARestar() * factor);
	}

	@Override
	public String toString() {
		return "ObjectesMillorar [factor=" + factor + ", nom=" + nom + ", tipus=" + tipus + "]";
	}

}
