package objetos;

public class ObjectesCacar implements Objecte {

	private String nom;
	private String tipus;
	private float puntsASumar;

	public ObjectesCacar(String nom, String tipus, float puntsASumar) {
		super();
		this.nom = nom;
		this.tipus = tipus;
		this.puntsASumar = puntsASumar;
	}

	@Override
	public String getNom() {
		return this.nom;
	}

	@Override
	public String getTipus() {
		return this.tipus;
	}

	public float getPuntsASumar() {
		return puntsASumar;
	}

	public void setPuntsASumar(float puntsASumar) {
		this.puntsASumar = puntsASumar;
	}

	@Override
	public String toString() {
		return "ObjectesCacar [nom=" + nom + ", tipus=" + tipus + ", puntsASumar=" + puntsASumar + "]";
	}
}
