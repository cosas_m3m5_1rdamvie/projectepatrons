package objetos;

import java.beans.PropertyChangeEvent;

import adapter.EnemicAdapterImpl;
import adapter.ObjecteBonusAdapterImpl;
import adapter.ObjecteCacarAdapterImpl;
import decorator.JugadorDecorator;
import observer.Jugador;
import observer.ObjecteGuanyat;

public class Guerrer implements Jugador {

	private String nom;
	private float puntos;
	private float factor;

	public Guerrer(String nom, float saldo) {
		this.nom = nom;
		this.puntos = saldo;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public float getPunts() {
		return this.puntos;
	}

	@Override
	public void setPunts(float punts) {
		this.puntos = punts;
	}

	@Override
	public float getFactor() {
		return this.factor;
	}

	@Override
	public void setFactor(float factor) {
		this.factor = factor;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		ObjecteGuanyat obj = (ObjecteGuanyat) evt.getNewValue();

		Jugador j = (Jugador) obj.getJugador();

		if (this.getNom().equals(j.getNom())) { // Comprovar si aquest és el personatge que agafa l'objecte
			float punts = 0;

			if (obj.getObjecteGuanyat() instanceof ObjectesCacar) { // És objecteCaçar?
				ObjectesCacar objecte = (ObjectesCacar) obj.getObjecteGuanyat();
				System.out.println("Objecte es objecte cacar");
				ObjecteCacarAdapterImpl oca = new ObjecteCacarAdapterImpl(objecte,  j); 
				punts = oca.getPuntsASumar();
			}

			else if (obj.getObjecteGuanyat() instanceof ObjectesBonus) { // És objecteBonus?
				ObjectesBonus objecte = (ObjectesBonus) obj.getObjecteGuanyat();
				System.out.println("Objecte es objecte bonus");
				ObjecteBonusAdapterImpl oba = new ObjecteBonusAdapterImpl(objecte, j); 
				punts = oba.getPuntsASumar();
				
			} else if (obj.getObjecteGuanyat() instanceof Enemics) { // És Enemic?
				Enemics enemic = (Enemics) obj.getObjecteGuanyat();
				System.out.println("Objecte es objecte enemic");
				EnemicAdapterImpl ea = new EnemicAdapterImpl(enemic, j); 
				punts = ea.getPuntsARestar();
				
			}
			
			System.out.println("Factor: "+this.factor);
			this.puntos += punts;
		}

	}

	@Override
	public String descripcio() {
		return this.toString();
	}

	@Override
	public String toString() {
		return "Guerrer [nom=" + nom + ", punts=" + puntos + "]";
	}

}
