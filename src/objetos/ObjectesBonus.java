package objetos;

import observer.Jugador;

public class ObjectesBonus implements Objecte {

	private String nom;
	private String tipus;
	private float puntsASumar;

	public ObjectesBonus(String nom, String tipus, int puntsASumar) {
		super();
		this.nom = nom;
		this.tipus = tipus;
		this.puntsASumar = puntsASumar;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public String getTipus() {
		return tipus;
	}

	public float getPuntsASumar() {
		return puntsASumar;
	}

	public void setPuntsASumar(int puntsASumar) {
		this.puntsASumar = puntsASumar;
	}

	public void ferServir(Jugador j) {
		j.setPunts(j.getPunts() + puntsASumar);
	}

	@Override
	public String toString() {
		return "ObjectesBonus [nom=" + nom + ", tipus=" + tipus + ", puntsASumar=" + puntsASumar + "]";
	}

}
