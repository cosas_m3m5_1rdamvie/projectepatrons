package objetos;

public class Vehicles implements Objecte {

	private String nom;
	private String tipus;

	public Vehicles(String nom, String tipus) {
		super();
		this.nom = nom;
		this.tipus = tipus;
	}

	@Override
	public String getNom() {
		return this.nom;
	}

	@Override
	public String getTipus() {
		return this.tipus;
	}

	@Override
	public String toString() {
		return "Vehicle [nom=" + nom + ", tipus=" + tipus + "]";
	}

}
