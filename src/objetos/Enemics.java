package objetos;

import observer.Jugador;

public class Enemics implements Objecte {

	private String nom;
	private String tipus;
	private float puntsARestar;

	public Enemics(String nom, String tipus, float puntsARestar) {
		super();
		this.nom = nom;
		this.tipus = tipus;
		this.puntsARestar = puntsARestar;
	}

	@Override
	public String getNom() {
		return this.nom;
	}

	@Override
	public String getTipus() {
		return this.tipus;
	}

	public float getPuntsARestar() {
		return puntsARestar;
	}

	public void setPuntsARestar(float puntsARestar) {
		this.puntsARestar = puntsARestar;
	}

	public void atacar(Jugador j) {
		j.setPunts(j.getPunts() - this.puntsARestar);
	}

	@Override
	public String toString() {
		return "Enemics [nom=" + nom + ", tipus=" + tipus + ", puntsARestar=" + puntsARestar + "]";
	}

}
