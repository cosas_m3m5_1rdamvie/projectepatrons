package factory;

import objetos.Comerciant;
import objetos.Guerrer;
import objetos.Mag;
import observer.Jugador;

public class JugadorFactory {

	public Jugador crearJugador(EnumJugador tipus, String nom, int puntuacio) {
		switch (tipus) {
		case GUERRER:
			return new Guerrer(nom, puntuacio);
		case MAG:
			return new Mag(nom, puntuacio);
		case COMERCIANT:
			return new Comerciant(nom, puntuacio);
		default:
			return null;
		}

	}
}
