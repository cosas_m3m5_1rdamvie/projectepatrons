package factory;

public class FactoryProvider {
	public static JocFactory getFactory(String choice) {

		switch (choice) {

		case "Enemics":
			return new EnemicsFactory();
		case "ObjectesCacar":
			return new ObjectesCacarFactory();
		case "ObjectesMillorar":
			return new ObjectesMillorarFactory();
		case "ObjectesBonus":
			return new ObjectesBonusFactory();
		case "Vehicles":
			return new VehiclesFactory();
		default:
			return null;
		}
	}
}
