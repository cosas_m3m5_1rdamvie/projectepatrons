package factory;

import objetos.Espasa;
import objetos.ExerciciEloi;
import objetos.Laxant;
import objetos.ObjectesMillorar;
import objetos.Pistola;

public class ObjectesMillorarFactory implements JocFactory<ObjectesMillorar> {

	@Override
	public ObjectesMillorar create(String type, String name) {
		switch (type) {
		case "Espasa":
			return new Espasa(name);
		case "Pistola":
			return new Pistola(name);
		case "Laxant":
			return new Laxant(name);
		case "ExerciciEloi":
			return new ExerciciEloi(name);
		default:
			return null;
		}
	}

}
