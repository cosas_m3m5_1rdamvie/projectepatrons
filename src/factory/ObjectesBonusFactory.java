package factory;

import objetos.Diamant;
import objetos.Hamburguesa;
import objetos.ObjectesBonus;
import objetos.Pocio;
import objetos.RedBull;

public class ObjectesBonusFactory implements JocFactory<ObjectesBonus> {

	@Override
	public ObjectesBonus create(String type, String name) {
		switch (type) {
		case "Hamburguesa":
			return new Hamburguesa(name);
		case "Diamant":
			return new Diamant(name);
		case "RedBull":
			return new RedBull(name);
		case "Pocio":
			return new Pocio(name);
		default:
			return null;
		}
	}

}
