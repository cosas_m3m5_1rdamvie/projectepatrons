package factory;

import objetos.CofreDelTresor;
import objetos.LingotOr;
import objetos.Moneda;
import objetos.ObjectesCacar;

public class ObjectesCacarFactory implements JocFactory<ObjectesCacar> {

	@Override
	public ObjectesCacar create(String type, String name) {
		switch (type) {
		case "Moneda":
			return new Moneda(name);
		case "LingotOr":
			return new LingotOr(name);
		case "CofreDelTresor":
			return new CofreDelTresor(name);
		default:
			return null;
		}
	}

}
