package factory;

public interface JocFactory<T> {
	T create(String type, String name);
}
