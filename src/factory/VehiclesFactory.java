package factory;

import objetos.AlaDelta;
import objetos.Barca;
import objetos.Cotxe;
import objetos.Drac;
import objetos.Patinet;
import objetos.Vehicles;

public class VehiclesFactory implements JocFactory<Vehicles> {

	@Override
	public Vehicles create(String type, String name) {
		switch (type) {
		case "Barca":
			return new Barca(name);
		case "Cotxe":
			return new Cotxe(name);
		case "Patinet":
			return new Patinet(name);
		case "Ala delta":
			return new AlaDelta(name);
		case "Drac":
			return new Drac(name);
		default:
			return null;
		}
	}

}
