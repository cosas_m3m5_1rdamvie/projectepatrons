package factory;

import objetos.Bestia;
import objetos.Enemics;
import objetos.Monstre;
import objetos.Pirata;

public class EnemicsFactory implements JocFactory<Enemics> {

	@Override
	public Enemics create(String type, String name) {
		switch (type) {
		case "Pirata":
			return new Pirata(name);
		case "Monstre":
			return new Monstre(name);
		case "Bestia":
			return new Bestia(name);
		default:
			return null;
		}
	}

}
