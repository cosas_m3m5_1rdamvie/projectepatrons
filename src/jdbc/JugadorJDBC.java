package jdbc;

public class JugadorJDBC implements Comparable{

	private String nom;
	private float punts;
	

	public JugadorJDBC(String nom, float punts) {
		super();
		this.nom = nom;
		this.punts = punts;
	}


	@Override
	public int compareTo(Object o) {
		return this.nom.compareTo(((JugadorJDBC) o).getNom());
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public float getPunts() {
		return punts;
	}


	public void setPunts(float punts) {
		this.punts = punts;
	}
	
}
