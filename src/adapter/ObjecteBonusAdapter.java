package adapter;

public interface ObjecteBonusAdapter {
	String getNom();

	String getTipus();

	float getPuntsASumar();

	void setPuntsASumar(int puntsASumar);
}
