package adapter;

import decorator.JugadorDecorator;
import objetos.Enemics;
import objetos.ObjectesMillorar;
import observer.Jugador;

public class EnemicAdapterImpl implements EnemicAdapter {

	private Enemics enemic;
	private Jugador j;

	public EnemicAdapterImpl(Enemics enemic, Jugador j) {
		super();
		this.enemic = enemic;
		this.j = j;
	}

	@Override
	public String getNom() {
		return enemic.getNom();
	}

	@Override
	public String getTipus() {
		return enemic.getTipus();
	}

	@Override
	public float getPuntsARestar() {
		return puntosConvertidos();
	}

	@Override
	public void setPuntsARestar(float puntsArestar) {
		enemic.setPuntsARestar(puntsArestar);
	}

	private float puntosConvertidos() {
		float resul = 0;
		if (j instanceof JugadorDecorator) { // És JugadorDecorator?
			JugadorDecorator jDecorator = (JugadorDecorator) j;
			resul = enemic.getPuntsARestar() * j.getFactor();
		} else { // NO és JugadorDecorator
			resul = enemic.getPuntsARestar();
		}
		return -resul;
	}

}
