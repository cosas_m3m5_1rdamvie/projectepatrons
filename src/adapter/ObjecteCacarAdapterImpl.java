package adapter;

import decorator.JugadorDecorator;
import objetos.ObjectesCacar;
import objetos.ObjectesMillorar;
import observer.Jugador;

public class ObjecteCacarAdapterImpl implements ObjecteCacarAdapter {

	private ObjectesCacar objC;
	private Jugador j;

	public ObjecteCacarAdapterImpl(ObjectesCacar objC, Jugador j) {
		super();
		this.objC = objC;
		this.j = j;
	}

	@Override
	public String getNom() {
		return objC.getNom();
	}

	@Override
	public String getTipus() {
		return objC.getTipus();
	}

	@Override
	public float getPuntsASumar() {
		return puntosConvertidos();
	}

	@Override
	public void setPuntsASumar(float puntsASumar) {
		objC.setPuntsASumar(puntsASumar);
	}

	private float puntosConvertidos() {
		float resul = 0;
		if (j instanceof JugadorDecorator) { // És JugadorDecorator?
			JugadorDecorator jDecorator = (JugadorDecorator) j;
			resul = objC.getPuntsASumar() / j.getFactor();
		} else { // NO és JugadorDecorator
			resul = objC.getPuntsASumar();
		}
		return resul;
	}

}
