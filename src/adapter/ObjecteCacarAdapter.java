package adapter;

public interface ObjecteCacarAdapter {
	String getNom();

	String getTipus();

	float getPuntsASumar();

	void setPuntsASumar(float puntsASumar);
}
