package adapter;

import decorator.JugadorDecorator;
import objetos.ObjectesBonus;
import objetos.ObjectesMillorar;
import observer.Jugador;

public class ObjecteBonusAdapterImpl implements ObjecteBonusAdapter {

	private ObjectesBonus objB;
	private Jugador j;

	public ObjecteBonusAdapterImpl(ObjectesBonus objB, Jugador j) {
		super();
		this.objB = objB;
		this.j = j;
	}

	@Override
	public String getNom() {
		return objB.getNom();
	}

	@Override
	public String getTipus() {
		return objB.getTipus();
	}

	@Override
	public float getPuntsASumar() {
		return puntosConvertidos();
	}

	@Override
	public void setPuntsASumar(int punts_a_sumar) {
		objB.setPuntsASumar(punts_a_sumar);
	}

	private float puntosConvertidos() {
		float resul = 0;
		if (j instanceof JugadorDecorator) { // És JugadorDecorator?
			JugadorDecorator jDecorator = (JugadorDecorator) j;
			resul = objB.getPuntsASumar() / j.getFactor();
		} else { // NO és JugadorDecorator
			resul = objB.getPuntsASumar();
		}
		return resul;
	}
}
