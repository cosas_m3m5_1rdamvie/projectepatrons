package adapter;

public interface EnemicAdapter {
	String getNom();

	String getTipus();

	float getPuntsARestar();

	void setPuntsARestar(float puntsArestar);
}
