module ProjectePatrons {
	exports factory;
	exports observer;
	exports decorator;
	exports adapter;
	exports main;
	exports objetos;

	requires java.desktop;
	requires java.sql;
	requires java.base;
}