# 🏴‍☠️ Juego _Caza del tesoro_ 🏴‍☠️

Desarrollo del juego _Caza del tesoro_ el cual consiste en que los jugadores controlarán a un personaje que tiene que recoger objetos del mundo del tesoro para ganar puntos. Estos objetos pueden ser monedas, diamantes, oro y otros objetos preciosos. El jugador también se puede encontrar con enemigos que le restarán puntos. Gana el jugador que obtenga más puntos.

## 📄 Descripción
Proyecto realizado para la asignatura "Acceso a datos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías 
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white)
- Interfície comparator.
- Patrones de diseño: Factory method, Abstract factory, Adapter, Decorator y Observer.
- JDBC para base de datos.

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el documento de pruebas del proyecto adjuntado a continuación: [Documento de pruebas](https://gitlab.com/slimmyteam/accesodatos/projectepatrons/-/blob/main/Activitat07_Document_de_proves.pdf).

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.